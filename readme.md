## Analysis results:
## Least/most profitable sections of customers
## Risk details best indicating claim frequency
## Risk details best indicating amount of premium 


## Data:

### Policy		
				
#### POLICY_NUMBER	Policy ID Number	
#### START_DATE	Policy Start Date	
#### END_DATE	Policy End Date	
#### POLICY_STATUS	Current Status of the Policy	
#### PREMIUM	Premium/Price paid by the customer for the policy	
#### NUMBER_OF_DRIVERS	Number of drivers named in the policy	
#### PAYMENTMETHOD	How the policy was paid for	
#### VEHICLE_ID	Vehicle ID Number	
#### CUSTOMER_ID	Customer ID Number	
		
		
### Customer		
		
#### CUSTOMER_ID	Customer ID	
#### MARITALSTATUS	Customer current relationship status	
#### DATE_OF_BIRTH	Date of Birth of the Policyholder	
#### NCD	No Claim Discount - Number of years a customers has not claimed on historic car insurance policies	
#### EMPLOYMENTTYPE	Type of Employment declared by the customer at start f policy	
#### VEHICLE_CLASSOFUSE	How the vehicle if to be used by the customer, according to what they declare at start of the policy	
#### 	Social inc. Comm	Using vehicle for social use Including travelling to 1 (per driver) destination for work purposes.
#### 	Social only	Using vehicle for social use only.
#### 	Business Use (PH)	Using vehicle for social use Including Policyholder travelling to more than 1 destination for work purposes.
#### 	Business use (PH + Spouse / Civil Partner)	Using vehicle for social use Including Policyholder & Spouse travelling to more than 1 destination #### for work purposes.
#### 	Business use by all drivers	Using vehicle for social use Including all drivers travelling to more than 1 destination for work purposes.
#### 	Business use (spouse / Civil Parnter)	Using vehicle for social use Including  Spouse travelling to more than 1 destination for work purposes.
		
### Vehicle		
		
#### VEHICLE_ID	Vehicle ID Number	
#### VEHICLE_VALUE	Value of Vehicle declared by customer at start of policy	
#### VEHICLE_AGE	Age  of Vehicle declared by customer at start of policy	
#### VEHICLE_ANNUALMILEAGE	Amount of Miles Customer expected to be doing during policy term	
#### VEHICLE_OWNEDMONTHS	Months customer have owned the vehicle declared at start of policy	
		
		
		
### Segment		
		
		
#### POLICY_NUMBER	Policy ID Number	
#### START_DATE	Policy Start Date	
#### SEGMENT_1_FLAG	Group  ID  of customers identified as specific target	
#### SEGMENT_2_FLAG	Group  ID  of customers identified as specific target	
#### SEGMENT_3_FLAG	Group  ID  of customers identified as specific target	
#### SEGMENT_4_FLAG	Group  ID  of customers identified as specific target	
#### SEGMENT_5_FLAG	Group  ID  of customers identified as specific target	
		
		
		
### Claims		
		
		
#### POLICY_NUMBER	Policy ID Number	
#### DATE_OF_ACCIDENT	Date when the incident that led to a claim occurred	
#### CLAIM_ID	Claim ID Number	
#### INCURRED	The cost of the claim	






		
